<?php

namespace Dpx\VodAuthKey;

class VodAuthKey
{
    protected $key;

    public function __construct($key = ''){
        $this->key = $key;
    }

    public function getAuthKeyUrl($url)
    {
        $key = $this->key;
        $time = time()+1800;
        $string = $url."-".$time."-0-0-";
        $md5hash = md5($string.$key);
        $authKeyUrl = $url.'?auth_key='.$time."-0-0-".$md5hash;

        return $authKeyUrl;
    }

}
